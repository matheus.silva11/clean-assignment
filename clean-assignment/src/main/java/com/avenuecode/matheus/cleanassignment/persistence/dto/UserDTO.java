package com.avenuecode.matheus.cleanassignment.persistence.dto;

import com.avenuecode.matheus.cleanassignment.persistence.entities.User;

public class UserDTO {

	private String name;
	private int age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public User transformToObject() {
		User user = new User();
		return user;
	}

}

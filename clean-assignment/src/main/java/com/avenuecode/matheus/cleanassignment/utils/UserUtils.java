package com.avenuecode.matheus.cleanassignment.utils;

import com.avenuecode.matheus.cleanassignment.persistence.dto.UserDTO;

public class UserUtils {
	
	public boolean validateUser(UserDTO user) {
		boolean validateAge = user.getAge() > 20;
		boolean validateName = !user.getName().isEmpty();
		
		boolean valid = validateAge && validateName ? true : false;
		
		return valid;
	}

}

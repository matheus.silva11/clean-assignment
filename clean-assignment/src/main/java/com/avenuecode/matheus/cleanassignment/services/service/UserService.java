package com.avenuecode.matheus.cleanassignment.services.service;

import com.avenuecode.matheus.cleanassignment.persistence.dto.UserDTO;
import com.avenuecode.matheus.cleanassignment.persistence.entities.User;

public interface UserService {

	User save(UserDTO user);

}

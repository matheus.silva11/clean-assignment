package com.avenuecode.matheus.cleanassignment.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenuecode.matheus.cleanassignment.persistence.dto.UserDTO;
import com.avenuecode.matheus.cleanassignment.persistence.entities.User;
import com.avenuecode.matheus.cleanassignment.persistence.repositories.UserRepository;
import com.avenuecode.matheus.cleanassignment.services.service.UserService;
import com.avenuecode.matheus.cleanassignment.utils.UserUtils;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired UserRepository repository;
	
	UserUtils utils = new UserUtils();

	public User save(UserDTO user) {
		if(utils.validateUser(user)) {
			return repository.save(user.transformToObject());
		}
		return null;
	}

}

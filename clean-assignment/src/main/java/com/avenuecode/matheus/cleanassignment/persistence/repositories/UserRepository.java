package com.avenuecode.matheus.cleanassignment.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.avenuecode.matheus.cleanassignment.persistence.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}

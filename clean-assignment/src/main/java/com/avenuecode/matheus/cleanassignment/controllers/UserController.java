package com.avenuecode.matheus.cleanassignment.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.matheus.cleanassignment.persistence.dto.UserDTO;
import com.avenuecode.matheus.cleanassignment.persistence.entities.User;
import com.avenuecode.matheus.cleanassignment.services.service.UserService;


@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired UserService service;
	
	@RequestMapping(value="/save" ,method = RequestMethod.POST)
	public User save(@RequestBody UserDTO user) {
		return service.save(user);
	}

}
